# frozen_string_literal: true

require "shops_parser/version"

module ShopsParser

  ##
  # This class identifies the category of item
  class Category < ActiveModel::Model
    def initialize
      p "Category created"
    end
  end

  ##
  # This class identifies the item
  class Item < ActiveModel::Model
    def initialize
      p "Item created"
    end
  end
end
